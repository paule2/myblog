package ml.news24.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ml.news24.domain.Category;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long>{
	Category findByCatid(int catid);
}
