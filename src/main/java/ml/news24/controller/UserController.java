package ml.news24.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ml.news24.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UserController {

	@Autowired
	private AuthenticationService authenticationService;
	
	
	@ResponseBody
	@RequestMapping(path	 = { "/user/{userid}" }, 
					method   = RequestMethod.POST, 
					produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public String userPost(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("image") MultipartFile image,
			@PathVariable int userid) {
		
		return authenticationService.postUser(request, response, image, userid);
	}
}
