package ml.news24.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ml.news24.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import ml.news24.domain.Blog;
import ml.news24.domain.User;


@Controller
public class BlogController {

	@Autowired
	private BlogService blogService;

	@RequestMapping(path={"/user/{userid}/new-blog"}, method=RequestMethod.GET)
	public String newBlogGet(HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable int userid) {
		return blogService.getNewBlog(request, response, model, userid);
	}
	
	/*
	 * Visualizza la pagina del blog
	 */
	@RequestMapping(path={"/user/{userid}/blog"}, method=RequestMethod.GET)
	public String getBlog(HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable int userid) {
		return blogService.getBlog(request, response, model, userid);
	}

	/*
	 * Cancella un post
	 */
	@RequestMapping(path={"/user/{userid}/delete-blog"}, method=RequestMethod.GET)
	public String deleteBlog(HttpServletRequest request, HttpServletResponse response, Model model,
						  @PathVariable int userid, @RequestParam(value = "deleteId", required = false) int deleteId) {


		return blogService.deleteBlog(request, response, model, userid, deleteId);
	}

	/*
	 * Gestisce la creazione di un nuovo blog
	 */
	@RequestMapping(path={"/user/{userid}/new-blog"}, method=RequestMethod.POST)
	public String postBlog(HttpServletRequest request, HttpServletResponse response, Model model,
			@PathVariable int userid,
			@RequestParam("blogCategory") int catid,
			@RequestParam("blogTitle") String title) {
		return blogService.postBlog(request, response, model, userid, catid, title);
	}

	public int validateBlogCreation(Blog targetBlog, User userSession, int userid) {
		return blogService.validateBlog(targetBlog, userSession, userid);
	}
}
