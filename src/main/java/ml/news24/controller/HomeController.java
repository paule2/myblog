package ml.news24.controller;

import javax.servlet.http.HttpServletRequest;

import ml.news24.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;


@Controller
public class HomeController {
	
	@Autowired
	private HomeService homeService;
	
	@RequestMapping(path={"/"}, method=RequestMethod.GET)
	public String homeGet(HttpServletRequest request, Model model) {
				
		return homeService.getHome(request, model);
	}

	@RequestMapping(
			path   = { "/home-search" },
			method = RequestMethod.GET
	)
	public String searchGet(HttpServletRequest request,
							@RequestParam("q") String query, Model model) {

		return homeService.listHome(request, model, query);
	}
}