package ml.news24.controller;

import javax.validation.Valid;

import ml.news24.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import ml.news24.domain.User;
import ml.news24.repository.UserRepository;

/*
 * Gestisce la registrazione degli utenti tramite chiamata AJAX
 */
@Controller
public class RegistrationController {
	
    @Autowired
    UserRepository userRepository;

	@Autowired
	private AuthenticationService authenticationService;


	@RequestMapping(path={"/signup"}, method=RequestMethod.GET)
	public String getRegistration() {
						  	      
		return "my_registrazione";
	}
	
	@ResponseBody
	@RequestMapping(
			path	 = { "/signup" }, 
			method	 = RequestMethod.POST,
			produces = { MediaType.APPLICATION_JSON_VALUE }
	)
	public String postRegistration(@ModelAttribute @Valid User user, BindingResult result) {
		return authenticationService.postRegistration(user, result);
	}
}
