package ml.news24.controller;

import javax.servlet.http.HttpServletRequest;

import ml.news24.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/*
 * Gestione dell'autenticazione degli utenti 
 */
@Controller
public class LoginController {

	@Autowired
	private AuthenticationService authenticationService;


	@RequestMapping(path = {"/login"}, method = RequestMethod.GET)
	public String getLogin() {
		return authenticationService.getLogin();
	}

	@RequestMapping(path = {"/login"}, method = RequestMethod.POST)
	public String postLogin(HttpServletRequest request, Model model,
							@RequestParam(value = "loginUsername") String username,
							@RequestParam(value = "loginPassword") String password) {

		return authenticationService.postLogin(request, model, username, password);
	}
}
