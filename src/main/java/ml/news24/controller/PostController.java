package ml.news24.controller;

import javax.servlet.http.HttpServletRequest;

import ml.news24.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class PostController {
	
	@Autowired
	private PostService postService;
	
	
	@RequestMapping(path={"/user/{userid}/blog/post/{postid}"}, method=RequestMethod.GET)
	public String newpostGet(HttpServletRequest request,
			Model model,
			@PathVariable int userid, 
			@PathVariable int postid) {
		
		return postService.getNewPost(request, model, userid, postid);
	}
	

	@RequestMapping(path={"/user/{userid}/blog/new-post"}, method=RequestMethod.POST)
	public String newpostPost(
			@PathVariable int userid,
			@RequestParam(value="postBody") String postBodyHtml,
			@RequestParam(value="postTitle") String postTitle,
			@RequestParam(value="postTags") String postTags) {
		
		return postService.postNewPost(userid, postBodyHtml, postTitle, postTags);
	}
}
