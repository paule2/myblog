package ml.news24.controller;

import javax.servlet.http.HttpServletRequest;

import ml.news24.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LogoutController {

	@Autowired
	private AuthenticationService authenticationService;

	@RequestMapping(path={"/logout"}, method=RequestMethod.GET)
	public String logout(HttpServletRequest request) {

		return  authenticationService.logout(request);
	}
}
