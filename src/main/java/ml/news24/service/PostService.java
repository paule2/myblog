package ml.news24.service;


import ml.news24.domain.Blog;
import ml.news24.domain.Post;
import ml.news24.domain.Tag;
import ml.news24.repository.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service
public class PostService {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private PostRepository postRepository;


    public String getNewPost(HttpServletRequest request,
                             Model model,
                             int userid,
                             int postid) {

        Post post = postRepository.findByPostid(postid);

        // update visualizzazioni
        post.setViews(post.getViews() + 1);
        postRepository.save(post);

        // lista di tag del post corrente
        List<Tag> tags = tagRepository.findByPostid(postid);

        // lista di post per ogni tag del commento
        List<List<Post>> posts_x_tag = new ArrayList<List<Post>>();
        List<List<String>> images 	 = new ArrayList<List<String>>();

        List<List<Long>> comments_count = new ArrayList<List<Long>>();



        for(Tag t : tags) {

            // tutti i post di un tag
            List<Post> tmp = postRepository.findByTagid(t.getTagid(), post.getPostid());

            List<String> images_x_tag = new ArrayList<String>();

            // tutte le immagini dei posts di un dato tag
            for(Post p : tmp) {
                images_x_tag.add(p.getFirstimage(request));
            }
            posts_x_tag.add(tmp);
            images.add(images_x_tag);
        }


        model.addAttribute("comments_count", comments_count);
        model.addAttribute("post", post);
        model.addAttribute("tags", tags);
        model.addAttribute("posts_x_tag", posts_x_tag);
        model.addAttribute("images", images);
        return "post_single";
    }


    public String postNewPost(
             int userid,
             String postBodyHtml,
             String postTitle,
             String postTags) {



        if(postTitle == "") {
            return "redirect:/user/{userid}/blog";
        }

        Blog blog = blogRepository.findByUserId(userid);

        List<Tag> tagList = null;
        List<String> tagListString = null;

        if(postTags != "") {
            tagListString = Arrays.asList(
                    postTags.split(",")
            );

            tagList = new ArrayList<>();

            // creazione lista di tag

            for(String s : tagListString) {

                s = s.trim().toLowerCase();

                Tag tmptag = tagRepository.findByTitle(s);

                if(tmptag == null) {

                    Tag t = new Tag(s);

                    System.out.println("Salvataggio tag");
                    tagRepository.save(t);

                    tagList.add(t);
                }
                else {
                    tagList.add(tmptag);
                }
            }
        }

        // salvataggio del post sul database

        String smalltitle = "";
        String smallposts = "";

        if(postTitle.length() > 35) {
            smalltitle = postTitle.substring(0, 31) + "...";
        }
        else {
            smalltitle = postTitle;
        }

        smallposts = Jsoup.clean(postBodyHtml, Whitelist.none());
        if(smallposts.length() > 133) {
            smallposts = smallposts.substring(0, 129) + "...";
        }

        Post post;
        if(postTags != "") {
            post = postRepository.save(new Post(
                    postTitle,
                    smalltitle,
                    Jsoup.clean(postBodyHtml, Whitelist.relaxed()),
                    smallposts,
                    blog,
                    tagList
            ));
        }
        else {
            post = postRepository.save(new Post(
                    postTitle,
                    smalltitle,
                    Jsoup.clean(postBodyHtml, Whitelist.relaxed()),
                    smallposts,
                    blog
            ));
        }

        return "redirect:/user/{userid}/blog/post/" + post.getPostid();
    }
}
