package ml.news24.service;


import ml.news24.domain.Post;
import ml.news24.domain.Tag;
import ml.news24.repository.PostRepository;
import ml.news24.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;


@Service
public class HomeService {

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private PostRepository postRepository;

    public String getHome(HttpServletRequest request, Model model) {

        List<String> images = new ArrayList<String>();
        List<Post> posts = postRepository.findTop10ByOrderByPubdateDesc();
        List<Tag> tags = (List<Tag>) tagRepository.findAll();

        for(Post p : posts) {
            images.add(p.getFirstimage(request));
        }

        model.addAttribute("tags", tags);
        model.addAttribute("images", images);
        model.addAttribute("posts", posts);

        return "post_list";
    }


    public String listHome(HttpServletRequest request, Model model, String tagTittle) {
        if(tagTittle.isEmpty()) {
            return getHome(request, model);
        }

        List<String> images = new ArrayList<String>();
        List<Post> posts = postRepository.findByTagslist_title(tagTittle);
        List<Tag> tags = (List<Tag>) tagRepository.findAll();


        for(Post p : posts) {
            images.add(p.getFirstimage(request));
        }

        model.addAttribute("tags", tags);
        model.addAttribute("images", images);
        model.addAttribute("posts", posts);
        model.addAttribute("q", tagTittle);

        return "post_list";
    }
}
