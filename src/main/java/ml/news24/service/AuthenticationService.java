package ml.news24.service;
import ml.news24.domain.User;
import ml.news24.exceptions.PasswordException;
import ml.news24.repository.UserRepository;
import org.imgscalr.Scalr;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.awt.image.BufferedImage;
import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class AuthenticationService {
    @Autowired
    private UserRepository userRepository;

    public String getLogin() {
        return "my_login";
    }

    public String postLogin(HttpServletRequest request,
                            Model model,
                            String username,
                            String password) {

        HttpSession session = request.getSession();

        User userSession = (User) userRepository.findByUsernameIgnoreCase(username);

        Map<String, String> JsonResp = new HashMap<String, String>();

        try {
            userSession.authenticate(password);

            session.setAttribute("userSession", userSession);

            return "redirect:/";
        }
        catch(NullPointerException e) {
            JsonResp.put("userErr", "* utente non identificato.");
        }
        catch(PasswordException e) {
            JsonResp.put("passErr", "* password non corretta");
        }
        model.addAttribute("errors", JsonResp);

        return "my_login";
    }

    public String logout(HttpServletRequest request) {

        HttpSession session = request.getSession();

        session.removeAttribute("userSession");

        return "redirect:/";
    }

    public String postRegistration(@ModelAttribute @Valid User user, BindingResult result) {

        JSONObject resp = new JSONObject();

        // eventuali errori di validazione
        if(result.hasErrors()) {

            resp.put("validated", false);

            JSONObject errors = new JSONObject();

            for(FieldError e : result.getFieldErrors()) {
                errors.put(e.getField(), e.getDefaultMessage());
            }

            resp.put("errorMessages", errors);
        }
        else{

            user.autoEncodePassword();

            try {
                userRepository.save(user);
                resp.put("validated", true);
            }
            catch(DataIntegrityViolationException e) {

                JSONObject errors = new JSONObject();

                if(e.getMostSpecificCause().getMessage().contains("username")) {
                    errors.put("username", "* utente già registrato.");
                }
                else {
                    errors.put("email", "* Email è già stata inserita.");
                }

                resp.put("validated", false);
                resp.put("errorMessages", errors);
            }
        }
        return resp.toString();
    }

    public String postUser(HttpServletRequest request, HttpServletResponse response,
                           MultipartFile image,
                           int userid) {

        JSONObject resp = new JSONObject();
        JSONObject ret  = new JSONObject();

        // controllo se l'utente ha inserito un file
        if(image.isEmpty()) {
            resp.put("validated", false);
            ret .put("fileType", "The image uploaded is empty.");
            resp.put("error", ret);

            return resp.toString();
        }

        String suffix 		  = LocalDateTime.now().toString()
                .replace('.', '-')
                .replace(':', '-');
        String mimeType 	  = null;
        String fileNameString = null;

        fileNameString = image.getOriginalFilename();
        mimeType 	   = request.getServletContext()
                .getMimeType(fileNameString);

        // controllo se il file inserito dall'utente � un'immagine
        if (!mimeType.startsWith("image/")) {

            // Return an error
            // ...
            resp.put("validated", false);
            ret .put("fileType", "The file uploaded is not an image.");
            resp.put("error", ret);

            return resp.toString();
        }

        BufferedImage buffImg = null;

        try {
            buffImg = ImageIO.read(new ByteArrayInputStream(image.getBytes()));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if(buffImg.getWidth() > 200) {
            ret.put("avatarResized", true);
        }

        // resize
        BufferedImage newAvatar = Scalr.resize(buffImg, 200);

        HttpSession session = request.getSession();

        User userSession = (User) session.getAttribute("userSession");

        String format = image.getOriginalFilename().substring(
                image.getOriginalFilename().lastIndexOf('.') + 1,
                image.getOriginalFilename().length()
        );

        File file = new File("D://server-uploads/" +
                userSession.getUserid() + '-' + suffix + "." + format
        );

        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            ImageIO.write(newAvatar, format, outputStream);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        resp.put("validated", true);

        userSession.setAvatar("/uploads/" + userSession.getUserid() + '-' + suffix + "." + format);

        userRepository.save(userSession);

        ret.put("avatarChange", true);
        ret.put("avatarPath", userSession.getAvatar());

        resp.put("success", ret);

        return resp.toString();
    }


    public String getUser(HttpServletRequest request, HttpServletResponse response, Model model,
                          int userid) {

        User user = userRepository.findByUserid(userid);

        if(user == null) {

            try {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not Found");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        List<String> images = new ArrayList<String>();


        model.addAttribute("userModel", user);
        model.addAttribute("images", images);

        return "user";
    }
}