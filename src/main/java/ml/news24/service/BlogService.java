package ml.news24.service;

import ml.news24.domain.*;
import ml.news24.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class BlogService {


    @Autowired
    private BlogRepository blogRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private PostRepository postRepository;


    public int validateBlog(Blog targetBlog, User userSession, int userid) {

        // il blog non � stato creato
        if(targetBlog == null) {
            // il blog da creare appartiene all'utente della sessione
            if(userSession != null &&
                    userid == userSession.getUserid()) {
                return 1;
            }
            return 2; // utente non loggato oppure utente loggato ma
            // il suo id non corrisponde al blog che vorrebbe creare
        }
        return 3;	  // blog gi� esistente
    }


    public String getNewBlog(HttpServletRequest request, HttpServletResponse response, Model model,
                             int userid) {

        if (!isAuthenticated(request)) {
            return "redirect:/";
        }

        Blog blog = blogRepository.findByUserId(userid);

        HttpSession session = request.getSession();

        // controlla se � possibile creare un blog
        int action = validateBlog(
                blog,
                (User) session.getAttribute("userSession"),
                userid
        );

        if(action == 2 || // il blog che voglio creare non � mio
                action == 3){  // esiste gia

            try {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        List<Category> categories = (List<Category>) categoryRepository.findAll();

        model.addAttribute("categories", categories);

        return "new_blog";
    }

    /*
     * Visualizza la pagina del blog
     */

    public String getBlog(HttpServletRequest request, HttpServletResponse response, Model model,
                          int userid) {

        if (!isAuthenticated(request)) {
            return "redirect:/";
        }

        Blog blog = blogRepository.findByUserId(userid);

        HttpSession session = request.getSession();
        
        User s2 = (User)session.getAttribute("userSession");

        // controlla se � possibile creare un blog
        int action = validateBlog(
                blog,
                s2,
                userid
        );

        request.getContextPath();

        if(action == 1){
            return "redirect:/user/{userid}/new-blog";
        }
        else if(action == 2) {

            try {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Not Found");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        List<Post> posts = postRepository.findByOwnerblog(blog);

        model.addAttribute("posts", posts);
        model.addAttribute("blog", blog);

        return "my_posts";
    }

    /*
     * Gestisce la creazione di un nuovo blog
     */
    public String postBlog(HttpServletRequest request, HttpServletResponse response, Model model,
                           int userid,
                           int catid,
                           String title) {

        if (!isAuthenticated(request)) {
            return "redirect:/";
        }

        HttpSession session = request.getSession();

        // controlla se � possibile creare un blog
        int action = validateBlog(
                blogRepository.findByUserId(userid),
                (User) session.getAttribute("userSession"),
                userid
        );

        if(action == 2 || // il blog che voglio creare non � mio
                action == 3){  // esiste gia

            try {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        Map<String, String> JsonResp = new HashMap<String, String>();

        boolean errors = false;

        // input validation
        if(title.length() < 1) {
            JsonResp.put("titleErr", "* insert a title.");
            errors = true;
        }

        // input not validated
        if(errors) {
            System.out.println(JsonResp);
            model.addAttribute("errors", JsonResp);
            return "blog-creation";
        }

        blogRepository.save(new Blog(
                title,
                (User) session.getAttribute("userSession"),
                categoryRepository.findByCatid(catid)
        ));

        return "redirect:/";
    }


    public String deleteBlog(HttpServletRequest request, HttpServletResponse response, Model model,
                          int userid, int deleteId) {

        if (!isAuthenticated(request)) {
            return "redirect:/";
        }

        if (deleteId != 0) {
            Post post = postRepository.findByPostid(deleteId);
            if (post != null) {
                List<Tag> tags = tagRepository.findByPostid(post.getPostid());
                postRepository.delete(post);
                if(tags != null) {
                    tagRepository.deleteAll(tags);
                }
            }
        }


        return getBlog(request, response, model, userid);
    }

    private boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession();


        try {
            User u = (User)session.getAttribute("userSession");

            if(u != null) {
                return true;
            }
        }catch (Exception e) {
            return false;
        }



        return false;
    }

}
