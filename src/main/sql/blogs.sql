CREATE TABLE USERS(
    USERID SERIAL PRIMARY KEY,
    USERNAME VARCHAR(100) UNIQUE,
    PASSWORD VARCHAR(500) NOT NULL,
    EMAIL VARCHAR(100) UNIQUE,
    AVATAR VARCHAR(500),
    NAME VARCHAR(100) NOT NULL,
    SURNAME VARCHAR(100) NOT NULL,
    DESCRIPTION VARCHAR(3000),
    ENABLED BOOLEAN NOT NULL,
    ADMIN BOOLEAN NOT NULL,
    CREATION DATE
);

CREATE UNIQUE INDEX username_lower_case on Users (lower(username));
CREATE UNIQUE INDEX email_lower_case 	on Users (lower(email));

CREATE TABLE CATEGORIES(
	CATID SERIAL PRIMARY KEY,
	TITLE VARCHAR(100) NOT NULL
);

CREATE TABLE BLOGS(
	BLOGID SERIAL PRIMARY KEY,
	TITLE VARCHAR(100) NOT NULL,
	CREATION DATE,
	USERID INT CONSTRAINT fk_userid REFERENCES Users      (userid) ON DELETE CASCADE,
	CATID  INT CONSTRAINT fk_catid  REFERENCES Categories (catid)  ON DELETE CASCADE
);

CREATE TABLE POSTS(
	POSTID SERIAL PRIMARY KEY,
	TITLE VARCHAR(100) NOT NULL,
	SMALLTITLE VARCHAR(100),
	POST VARCHAR(10000) NOT NULL,
	SMALLPOST VARCHAR(200),
	PUBDATE TIMESTAMP NOT NULL,
	VIEWS INT,
	BLOGID INT CONSTRAINT fk_blogid REFERENCES Blogs (blogid) ON DELETE CASCADE
);

CREATE TABLE TAGS(
	TAGID SERIAL PRIMARY KEY,
	TITLE VARCHAR(100) NOT NULL
);

CREATE TABLE POSTS_TAGS(
	POSTID INT CONSTRAINT fk_postid REFERENCES Posts (postid),
	TAGID  INT CONSTRAINT fk_tagid  REFERENCES Tags (tagid)
);

insert into categories (title) VALUES ('Arts & Humanities');
insert into categories (title) VALUES ('Cars & Transportation');
insert into categories (title) VALUES ('Dining Out');
insert into categories (title) VALUES ('Environment');
insert into categories (title) VALUES ('Games & Recreation');
insert into categories (title) VALUES ('Local Businesses');
insert into categories (title) VALUES ('Politics & Government');
insert into categories (title) VALUES ('Social Science');
insert into categories (title) VALUES ('Travel');
insert into categories (title) VALUES ('Beauty & Style');
insert into categories (title) VALUES ('Computers & Internet');
insert into categories (title) VALUES ('Education & Reference');
insert into categories (title) VALUES ('Family & Relationships');
insert into categories (title) VALUES ('Health');
insert into categories (title) VALUES ('News & Events');
insert into categories (title) VALUES ('Pregnancy & Parenting');
insert into categories (title) VALUES ('Society & Culture');
insert into categories (title) VALUES ('Business & Finance');
insert into categories (title) VALUES ('Consumer Electronics');
insert into categories (title) VALUES ('Entertainment & Music');
insert into categories (title) VALUES ('Food & Drink');
insert into categories (title) VALUES ('Home & Garden');
insert into categories (title) VALUES ('Pets');
insert into categories (title) VALUES ('Science & Mathematics');
insert into categories (title) VALUES ('Sports');
insert into categories (title) VALUES ('Misc');
