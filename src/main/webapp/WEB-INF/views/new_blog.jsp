    <%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page isELIgnored="false" %>

<layout:extends name="mybase.jsp">


    <layout:put block="content">
        <div class="container-fluid pb-4 pt-4 paddding">
            <div class="container paddding">
                <div class="row mx-0">
                    <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                        <div>
                            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">New Blog</div>
                        </div>

                        <form id="commentform" method="POST"
                              action="${pageContext.request.contextPath}/user/${userSession.userid}/new-blog">
                            <h5>Title:</h5>
                            <input name="blogTitle" type="text" value="">
                            <hr>
                            <h5>Category:</h5>
                            <select name="blogCategory" class="custom-select" id="inputGroupSelect">
                                <c:forEach items="${categories}" var="item" varStatus="loop">
                                    <option value="${loop.count}">${item.title}</option>
                                </c:forEach>
                            </select>
                            <hr>
                            <input class="btn btn-success"  type="submit" value="Done">
                        </form>
                    </div>
                    <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">


                    </div>
                </div>
            </div>
        </div>
        </div>

    </layout:put>

</layout:extends>

