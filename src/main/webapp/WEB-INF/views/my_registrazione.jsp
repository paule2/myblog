<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Kodinger">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>My Login Page &mdash; Bootstrap 4 Login Page Snippet</title>
    <link href='<c:url value="/resources/auth/bootstrap/css/bootstrap.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="resources/auth/css/my-login.css"/>' rel='stylesheet' type='text/css' />
</head>

<body class="my-login-page">
<section class="h-100">
    <div class="container h-100">
        <div class="row justify-content-md-center h-100">
            <div class="card-wrapper">
                <div class="brand">
                  
                </div>
                <div class="card fat">
                    <div class="card-body">
                        <h4 class="card-title">Registrazione</h4>
                        <form method="POST"  name="signupForm" action="signup">

                            <div class="form-group">
                                <label for="first_name">Nome</label>

                                <input id="first_name" type="text" class="form-control" name="name" value="" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="last_name">Cognome</label>

                                <input id="last_name" type="text" class="form-control" name="surname" value="" required autofocus>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>

                                <input id="email" type="email" class="form-control" name="email" value="" required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="username">Nome Utente</label>
                                <input id="username" type="text" class="form-control" name="username" required data-eye>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input id="password" type="password" class="form-control" name="password" required data-eye>
                            </div>


                            <div class="form-group no-margin">
                                <button id="signupBtn" name="signupBtn" type="submit" class="btn btn-primary btn-block">
                                    Crea il tuo account
                                </button>
                            </div>
                            <div class="margin-top20 text-center">
                               Nuovo Utente? <a href="${pageContext.request.contextPath}/login">Login</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="footer">
                    Copyright &copy; 2018
                </div>
            </div>
        </div>
    </div>
</section>
<script src='<c:url value="resources/auth/js/jquery.min.js"/>'> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

<script src='<c:url value="resources/auth/bootstrap/js/bootstrap.js"/>'> </script>
<script>
    $(document).ready(function() {
        /*  Submit form using Ajax */
        $('#signupBtn').click(function(e) {


            //Prevent default submission of form
            e.preventDefault();

            //Remove all errors
            $('.error').remove();
            $('input[class=signup]').css('border', '1px solid #333');

            $.ajax({
                type: 'POST',
                url : window.location.origin + '/news24/signup',
                data : $('form[name=signupForm]').serialize(),
                    success : function(res) {

                    if(res.validated){
                        //Set response
                        $('#resultContainer pre code').text(JSON.stringify(res.employee));
                        $('#resultContainer').show();

                        setTimeout(function(){
                            window.location.href = window.location.origin + '/news24/'
                        }, 2000);

                    }else{

                        console.log(JSON.stringify(res));

                        //Set error messages
                        $.each(res.errorMessages,function(key,value){
                            $('input[name='+key+']').after('<span style="color: red" class="error">'+value+'</span>');
                            $('input[name='+key+']').css('border', '1px solid red');
                        });
                    }
                }
            })
        });
    });
</script>
</body>

</html>