<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page isELIgnored="false" %>

<layout:extends name="mybase.jsp">
	<layout:put block="content">
		<div class="main">
			<div class="container">
				<div class="error-404 text-center">
					<%--<div class="col-12">--%>
						<p>this link is a dead link</p>
						<img class="img-responsive" src='<c:url value="/resources/images/404.png" />' />
						<br>
					<%--</div>--%>
				</div>
			</div>
		</div>
	</layout:put>
</layout:extends>