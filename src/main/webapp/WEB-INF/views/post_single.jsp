<%--
  Created by IntelliJ IDEA.
  User: osemm
  Date: 6/18/2018
  Time: 8:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page isELIgnored="false" %>

<!DOCTYPE html>

<html lang="en" class="no-js">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>paule blog</title>
    <link href='<c:url value="/resources/mycss/media_query.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/bootstrap.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/animate.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/owl.carousel.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/owl.theme.default.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/style_1.css"/>' rel='stylesheet' type='text/css' />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
          crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <script src='<c:url value="/resources/myjs/modernizr-3.5.0.min.js"/>'> </script>

    <script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>

    <style>
        .single #fh5co-title-box {
            position: relative;
            height: 300px;
            width: 100%;
        }
    </style>
</head>

<body class="single">
<div class="container-fluid fh5co_header_bg">
    <div class="container">
        <div class="row">
            <div class="col-12 fh5co_mediya_center">
                <a href="#" class="color_fff fh5co_mediya_setting">
                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;Friday, 5 January 2018</a>
                <div class="d-inline-block fh5co_trading_posotion_relative">
                    <a href="#" class="treding_btn">Trending</a>
                    <div class="fh5co_treding_position_absolute"></div>
                </div>
                <a href="#" class="color_fff fh5co_mediya_setting">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 fh5co_padding_menu">
                <img src='<c:url value="/resources/myimages/logo.png" />' class="fh5co_logo_width" />
            </div>
            <div class="col-12 col-md-9 align-self-center fh5co_mediya_right">
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-search"></i>
                        </div>
                    </a>
                </div>
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-linkedin"></i>
                        </div>
                    </a>
                </div>
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-google-plus"></i>
                        </div>
                    </a>
                </div>
                <div class="text-center d-inline-block">
                    <a href="https://twitter.com/fh5co" target="_blank" class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-twitter"></i>
                        </div>
                    </a>
                </div>
                <div class="text-center d-inline-block">
                    <a href="https://fb.com/fh5co" target="_blank" class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-facebook"></i>
                        </div>
                    </a>
                </div>
                <!--<div class="d-inline-block text-center"><img src="images/country.png" alt="img" class="fh5co_country_width"/></div>-->
                <div class="d-inline-block text-center dd_position_relative ">
                    <c:choose>
                        <c:when test="${empty userSession}">
                            <div class="form-control fh5co_text_select_option">
                                <a class="nav-link" href="${pageContext.request.contextPath}/login">Login
                                    <span class="sr-only">(current)</span>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="form-control fh5co_text_select_option">
                                <a class="nav-link" href="${pageContext.request.contextPath}/logout">Logout
                                    <span class="sr-only">(current)</span>
                                </a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid bg-faded fh5co_padd_mediya padding_786">
    <div class="container padding_786">
        <nav class="navbar navbar-toggleable-md navbar-light ">
            <button class="navbar-toggler navbar-toggler-right mt-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src='<c:url value="/resources/myimages/logo.png" />' class="mobile_logo_width" />
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">

                        <a class="nav-link" href="${pageContext.request.contextPath}/">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <c:if test = "${not empty userSession}">
                        <li class="nav-item ">
                            <a class="nav-link" href="${pageContext.request.contextPath}/user/${userSession.userid}/blog">Blog <span class="sr-only">(current)</span></a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </nav>
    </div>
</div>

<div id="fh5co-title-box" style="background-image: url(images/camila-cordeiro-114636.jpg); background-position: 50% 90.5px;"
     data-stellar-background-ratio="0.5">
    <div class="overlay"></div>
    <div class="page-title">
        <img src="images/person_1.jpg" alt="Free HTML5 by FreeHTMl5.co">
        <span>${post.pubdate}</span>
        <h2>${post.title}</h2>
    </div>
</div>

<div id="fh5co-single-content" class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                <p>
                    ${post.post}
                </p>
            </div>
            <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Tags</div>
                </div>
                <div class="clearfix"></div>
                <div class="fh5co_tags_all">
                    <c:forEach items="${tags}" var="t" varStatus="loop">
                        <a href="#" class="fh5co_tagg">${t.title}</a>
                    </c:forEach>
                </div>
                <div>
                    <div class="fh5co_heading fh5co_heading_border_bottom pt-3 py-2 mb-4">Most Popular</div>
                </div>

                <c:forEach items="${tags}" var="t" varStatus="loop01">
                    <c:forEach items="${posts_x_tag[loop01.index]}" var="pt" varStatus="loop02">
                        <c:set var="path" value="${pageContext.request.contextPath}/user/${pt.ownerblog.owner.userid}/blog/post/${pt.postid}" />

                        <div class="row pb-3">
                            <div class="col-5 align-self-center">
                                <a href="${path}">
                                    <img src="${images[loop01.index][loop02.index]}" alt="img"
                                         class="fh5co_most_trading" /> </a>
                            </div>
                        </div>
                    </c:forEach>
                </c:forEach>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid fh5co_footer_bg pb-3">
    <div class="container animate-box">
        <div class="row">
            <div class="col-12 spdp_right py-5"></div>
            <div class="clearfix"></div>
            <div class="col-12 col-md-4 col-lg-3">
                <div class="footer_main_title py-3"> About</div>
                <div class="footer_sub_about pb-3"> Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                    unknown printer took a galley of type and scrambled it to make a type specimen book.
                </div>

            </div>
            <div class="col-12 col-md-6 col-lg-2">
                <div class="footer_main_title py-3"> Categoria</div>
                <ul class="footer_menu">
                    <li><a href="#" class=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp; Business</Business></a></li>
                    <li><a href="#" class=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp; Ambiente</a></li>
                    <li><a href="#" class=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp; Salute</a></li>
                </ul>
            </div>

        </div>

    </div>
</div>


<div class="container-fluid fh5co_footer_right_reserved">
    <div class="container">
        <div class="row  ">
            <div class="col-12 col-md-6 py-4 Reserved"> © Copyright 2018. </div>
        </div>
    </div>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop">
        <i class="fa fa-arrow-up"></i>
    </a>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script src='<c:url value="/resources/myjs/owl.carousel.min.js"/>'> </script>

<!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>
<!-- Waypoints -->
<script src='<c:url value="/resources/myjs/jquery.waypoints.min.js"/>'> </script>
<!-- Main -->
<script src='<c:url value="/resources/myjs/main.js"/>'> </script>

</body>

</html>