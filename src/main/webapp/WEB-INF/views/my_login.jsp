
<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page isELIgnored="false" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="author" content="Kodinger">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>My Login Page &mdash; Bootstrap 4 Login Page Snippet</title>
    <link href='<c:url value="/resources/auth/bootstrap/css/bootstrap.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="resources/auth/css/my-login.css"/>' rel='stylesheet' type='text/css' />

    <script src='<c:url value="resources/auth/js/jquery.min.js"/>'> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src='<c:url value="resources/auth/bootstrap/js/bootstrap.js"/>'> </script>


    <script type="text/javascript">
        function userErrors(){

            var err = "<c:out value='${errors.userErr}'/>";

            $('input[name=loginUsername]').after('<br><span style="color: red" class="error">'+err+'</span>');
            $('input[name=loginUsername]').css('border', '1px solid red');
        }

        function passErrors(){
            var err = "<c:out value='${errors.passErr}'/>";

            $('input[name=loginPassword]').after('<br><span style="color: red" class="error">'+err+'</span>');
            $('input[name=loginPassword]').css('border', '1px solid red');
        }
    </script>
</head>
<body class="my-login-page">
<section class="h-100">
    <div class="container h-100">
        <div class="row justify-content-md-center h-100">
            <div class="card-wrapper">
                <div class="brand">

                </div>
                <div class="card fat">
                    <div class="card-body">
                        <h4 class="card-title" style="text-align: center">Login</h4>
                        <form method="POST" name="loginForm" action="login">


                            <div class="form-group">
                                <label for="username">Nome Utente</label>
                                <input id="username" name="loginUsername" type="text" class="form-control" name="name" value="" required autofocus>
                                <c:if test="${not empty errors.userErr}">
                                    <script> userErrors() </script>
                                </c:if>
                            </div>


                            <div class="form-group">
                                <label for="password">Password</label>
                                <input id="password" name="loginPassword" type="password" class="form-control" name="password" required data-eye>
                                <c:if test="${not empty errors.passErr}">
                                    <script> passErrors() </script>
                                </c:if>
                            </div>



                            <div class="form-group no-margin">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Accedi
                                </button>
                            </div>
                            <div class="margin-top20 text-center">
                                <a href="${pageContext.request.contextPath}/signup">Registrati</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="footer">
                    Copyright &copy; 2018
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>