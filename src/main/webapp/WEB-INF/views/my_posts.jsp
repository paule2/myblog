
<%@page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page isELIgnored="false" %>

<!DOCTYPE HTML>
<html>

<head>
    <title>Blogging</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Blogger Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android  Compatible web template,
		Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />



    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>paule blog</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link href='<c:url value="/resources/mycss/media_query.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/bootstrap.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/animate.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/owl.carousel.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/owl.theme.default.css"/>' rel='stylesheet' type='text/css' />
    <link href='<c:url value="/resources/mycss/style_1.css"/>' rel='stylesheet' type='text/css' />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
          crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">

    <script src='<c:url value="/resources/myjs/modernizr-3.5.0.min.js"/>'> </script>





    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar(){
            window.scrollTo(0,1);
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({ scrollTop: $(this.hash).offset().top }, 900);
            });
        });
    </script>
    <script src='<c:url value="/resources/myjs/jquery.waypoints.min.js"/>'> </script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"
    />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css"
    />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1/css/froala_style.min.css" rel="stylesheet" type="text/css"
    />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/froala-editor/2.5.1//js/froala_editor.pkgd.min.js"></script>

    <script>
        $(function () {
            $('textarea').froalaEditor()
        });
    </script>

    <script>
        $(document).ready(function () {
            $('input[id=titlePost]').keyup(function (e) {
                if ($(this).val() != "") {
                    $(this).css('border', '1px solid #ccc');
                    $('span[class=error]').hide();
                    $('button[id=btnSuccess]').prop("disabled", false);
                }
                else {
                    $(this).css('border', '1px solid red');
                    $('span[class=error]').show();
                    $('button[id=btnSuccess]').prop("disabled", true);
                }
            });
        });
    </script>


</head>

<body>
<div class="container-fluid fh5co_header_bg">
    <div class="container">
        <div class="row">
            <div class="col-12 fh5co_mediya_center">
                <a href="#" class="color_fff fh5co_mediya_setting">
                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;&nbsp;Friday, 5 January 2018</a>
                <div class="d-inline-block fh5co_trading_posotion_relative">
                    <a href="#" class="treding_btn">Trending</a>
                    <div class="fh5co_treding_position_absolute"></div>
                </div>
                <a href="#" class="color_fff fh5co_mediya_setting">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</a>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-3 fh5co_padding_menu">
                <img src='<c:url value="/resources/myimages/logo.png" />' class="fh5co_logo_width" />
            </div>
            <div class="col-12 col-md-9 align-self-center fh5co_mediya_right">
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-search"></i>
                        </div>
                    </a>
                </div>
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-linkedin"></i>
                        </div>
                    </a>
                </div>
                <div class="text-center d-inline-block">
                    <a class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-google-plus"></i>
                        </div>
                    </a>
                </div>

                <div class="text-center d-inline-block">
                    <a href="https://fb.com/fh5co" target="_blank" class="fh5co_display_table">
                        <div class="fh5co_verticle_middle">
                            <i class="fa fa-facebook"></i>
                        </div>
                    </a>
                </div>
                <!--<div class="d-inline-block text-center"><img src="images/country.png" alt="img" class="fh5co_country_width"/></div>-->
                <div class="d-inline-block text-center dd_position_relative ">
                    <c:choose>
                        <c:when test="${empty userSession}">
                            <div class="form-control fh5co_text_select_option">
                                <a class="nav-link" href="${pageContext.request.contextPath}/login">Login
                                    <span class="sr-only">(current)</span>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="form-control fh5co_text_select_option">
                                <a class="nav-link" href="${pageContext.request.contextPath}/logout">Logout
                                    <span class="sr-only">(current)</span>
                                </a>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid bg-faded fh5co_padd_mediya padding_786">
    <div class="container padding_786">
        <nav class="navbar navbar-toggleable-md navbar-light ">
            <button class="navbar-toggler navbar-toggler-right mt-3" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img src='<c:url value="/resources/myimages/logo.png" />' class="mobile_logo_width" />
            </a>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">

                        <a class="nav-link" href="${pageContext.request.contextPath}/">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <c:if test = "${not empty userSession}">
                        <li class="nav-item ">
                            <a class="nav-link" href="${pageContext.request.contextPath}/user/${userSession.userid}/blog">Blog <span class="sr-only">(current)</span></a>
                        </li>
                    </c:if>
                </ul>
            </div>
        </nav>
    </div>
</div>

<div class="container-fluid pb-4 pt-4 paddding">
    <div class="container paddding">
        <div class="row mx-0">
            <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                <div>

                    <h3 class="tittle">${blog.title} <i class="glyphicon glyphicon-picture"></i></h3>
                    <h5 class="tittle">${blog.category.title}</h5>
                </div>


                <c:forEach items="${posts}" var="p" varStatus="loop">
                    <c:set var = "path" value = "${pageContext.request.contextPath}/user/${p.ownerblog.owner.userid}/blog/post/${p.postid}"/>

                    <div class="row pb-4">
                        <div class="col-md-5">
                            <div class="fh5co_hover_news_img">
                                <div class="fh5co_news_img"><img src='<c:url value="${images[loop.index]}"/>' alt=""/></div>
                                <div></div>
                            </div>
                        </div>
                        <div class="col-md-7 animate-box">
                            <a href="${path}" class="fh5co_magna py-2"> ${p.smalltitle}</a>

                            <p>${p.pubdate}</p>
                            <p>${p.smallpost}</p>
                            <div>

                                <a class="span_link" href="#">
                                    <span class="glyphicon glyphicon-eye-open"></span>
                                    views: ${p.views}
                                </a>
                                <br>
                                <a class="span_link" href="${pageContext.request.contextPath}/user/${userSession.userid}/delete-blog?deleteId=${p.postid}">
                                    <span style="color: red"><i class="fa fa-trash-o fa-2x" aria-hidden="true"></i></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                <%--<div class="clearfix"></div>--%>
                <div class="fh5co_tags_all">
                    <c:forEach items="${tags}" var="t" varStatus="loop">
                        <%--<c:set var = "path" value = "${pageContext.request.contextPath}/user/${p.ownerblog.owner.userid}/blog/post/${p.postid}"/>--%>
                        <a href="#" class="fh5co_tagg"> ${t.title}</a>
                    </c:forEach>
                </div>
                <c:if test = "${not empty userSession and userSession.userid == blog.owner.userid}">
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Nuovo Post</button>
                </c:if>
            </div>
        </div>

    </div>
</div>



<di><!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Creare un Post</h4>
                </div>
                <div class="modal-body">

                    <form id="froala-form" method="POST" action="${pageContext.request.contextPath}/user/${userSession.userid}/blog/new-post">

                        <label>Title:</label>
                        <input style="border: 1px solid red;" type="text" id="titlePost" class="form-control input-lg glyphicon-remove" name="postTitle"
                               value="">
                        <span class="error">* INSERISCI UN TITOLO PER IL TUO POST.</span>
                        <br>
                        <label>Tags:</label>
                        <input type="text" class="form-control input-lg" name="postTags">
                        <span class="warning">* Usa una virgola tra un tag e l'altro</span>

                        <hr>

                        <textarea name="postBody" id="froala-editor"></textarea>

                        <br>
                        <button type="submit" id="btnSuccess" class="btn btn-success" disabled="disabled">Crea</button>
                        <button type="submit" class="btn btn-failure" data-dismiss="modal">Cancella</button>
                    </form>

                </div>
            </div>
        </div>
    </div></di>

<div class="container-fluid fh5co_footer_bg pb-3">
    <div class="container animate-box">
        <div class="row">
            <div class="col-12 spdp_right py-5"></div>
            <div class="clearfix"></div>
            <div class="col-12 col-md-4 col-lg-3">
                <div class="footer_main_title py-3"> About</div>
                <div class="footer_sub_about pb-3"> Lorem Ipsum is simply dummy text of the printing and typesetting
                    industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                    unknown printer took a galley of type and scrambled it to make a type specimen book.
                </div>

            </div>
            <div class="col-12 col-md-6 col-lg-2">
                <div class="footer_main_title py-3"> Categoria</div>
                <ul class="footer_menu">
                    <li><a href="#" class=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp; Business</Business></a></li>
                    <li><a href="#" class=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp; Ambiente</a></li>
                    <li><a href="#" class=""><i class="fa fa-angle-right"></i>&nbsp;&nbsp; Salute</a></li>
                </ul>
            </div>

        </div>

    </div>
</div>


<div class="container-fluid fh5co_footer_right_reserved">
    <div class="container">
        <div class="row  ">
            <div class="col-12 col-md-6 py-4 Reserved"> © Copyright 2018. </div>
        </div>
    </div>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="fa fa-arrow-up"></i></a>
</div>

<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--%>

<script src='<c:url value="/resources/myjs/owl.carousel.min.js"/>'> </script>

<!--<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
        integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
        crossorigin="anonymous"></script>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"
        integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn"
        crossorigin="anonymous"></script>

<!-- Main -->
<script src='<c:url value="/resources/myjs/main.js"/>'> </script>

</body>
</html>
