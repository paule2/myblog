<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://kwonnam.pe.kr/jsp/template-inheritance" prefix="layout"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page isELIgnored="false" %>

<layout:extends name="mybase.jsp">


    <layout:put block="content">
        <div class="container-fluid pb-4 pt-4 paddding">
            <div class="container paddding">
                <div class="row mx-0">
                    <div class="col-md-8 animate-box" data-animate-effect="fadeInLeft">
                        <div>
                            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">All Posts</div>
                        </div>

                        <c:forEach items="${posts}" var="p" varStatus="loop">
                        <c:set var = "path" value = "${pageContext.request.contextPath}/user/${p.ownerblog.owner.userid}/blog/post/${p.postid}"/>
                            <div class="row pb-4">
                                <div class="col-md-5">
                                    <div class="fh5co_hover_news_img">
                                        <div class="fh5co_news_img"><img src='<c:url value="${images[loop.index]}"/>' alt=""/></div>
                                        <div></div>
                                    </div>
                                </div>
                                <div class="col-md-7 animate-box">
                                    <a href="${path}" class="fh5co_magna py-2"> ${p.smalltitle}</a> <a href="${pageContext.request.contextPath}/user/${p.ownerblog.owner.userid}/blog/post/${p.postid}" class="fh5co_mini_time py-3"> ${p.pubdate}</a>
                                    <div class="fh5co_consectetur"> ${p.smallpost}</div>

                                </div>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="col-md-3 animate-box" data-animate-effect="fadeInRight">
                        <div>
                            <div class="fh5co_heading fh5co_heading_border_bottom py-2 mb-4">Tags


                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="fh5co_tags_all">
                            <c:forEach items="${tags}" var="t" varStatus="loop">
                            <%--<c:set var = "path" value = "${pageContext.request.contextPath}/user/${p.ownerblog.owner.userid}/blog/post/${p.postid}"/>--%>
                                <a href="${pageContext.request.contextPath}/home-search?q=${t.title}" class="fh5co_tagg"> ${t.title}</a>
                            </c:forEach>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        </div>

</layout:put>

</layout:extends>

